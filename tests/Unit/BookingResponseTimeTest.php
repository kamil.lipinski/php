<?php

namespace Tests\Unit;

use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    protected $calculator;
    protected $officeHours;

    protected function setUp()
    {
        parent::setUp();
        $this->calculator = new ResponseTimeCalculator();
        $this->officeHours = json_decode('[{"to":"19:00","from":"9:00","isClosed":false},{"to":"18:00","from":"8:00","isClosed":false},{"isClosed":true},{"to":"19:00","from":"10:00","isClosed":false},{"to":"19:00","from":"11:00","isClosed":false},{"to":"17:00","from":"12:00","isClosed":false},{"to":"15:00","from":"11:00","isClosed":false}]', true);
    }

    /** @test */
    public function testTask4SameDay()
    {
        // we also could use database but it's better to use hardcoded values

        $this->assertEquals(1,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-03 08:01', $this->officeHours));

        $this->assertEquals(0,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-03 08:00', $this->officeHours));

        $this->assertEquals(598,
            $this->calculator->calculate('2018-09-03 08:01', '2018-09-03 17:59', $this->officeHours));

        $this->assertEquals(599,
            $this->calculator->calculate('2018-09-03 00:00', '2018-09-03 17:59', $this->officeHours));

        $this->assertEquals(600,
            $this->calculator->calculate('2018-09-03 00:00', '2018-09-03 18:01', $this->officeHours));

        $this->assertEquals(0,
            $this->calculator->calculate('2018-09-04 02:00', '2018-09-04 18:01', $this->officeHours));

        $this->expectException(\InvalidArgumentException::class);
        $this->assertEquals(1,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-03 07:58', $this->officeHours));

    }

    public function testTask4ManyDays()
    {
        $this->assertEquals(601,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-05 10:01', $this->officeHours));
        $this->assertEquals(601,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-05 10:01', $this->officeHours));
        $this->assertEquals(600,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-05 10:00', $this->officeHours));
        $this->assertEquals(600 + 540,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-05 20:00', $this->officeHours));

        $minutesOpenInOneWeek = 600 + 600 + 540 + 480 + 300 + 240;

        $this->assertEquals(600 + 538 + $minutesOpenInOneWeek,
            $this->calculator->calculate('2018-09-03 07:59', '2018-09-12 18:58', $this->officeHours));

    }


}
