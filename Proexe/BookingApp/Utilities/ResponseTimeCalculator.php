<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use \Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use \Carbon\Carbon;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{

    /**
     * Calculates how many minutes office is open on each day of a week
     *
     * @param $officeHours
     * @return array
     */
    protected function officeHoursToMinutes($officeHours)
    {
        $minutesOpen = [];

        for ($i = 0; $i < 7; $i++) {
            $dayOfficeHours = $officeHours[$i];
            $minutesOpen[$i] = $dayOfficeHours['isClosed'] ? 0 :
                (new Carbon($dayOfficeHours['to']))->diffInMinutes($dayOfficeHours['from']);
        }

        return $minutesOpen;
    }

    /**
     * Calculates response time for the booking
     *
     * @param $bookingDateTime
     * @param $responseDateTime
     * @param $officeHours
     *
     * @throws \InvalidArgumentException
     *
     * @return int
     */
    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        $bookingDateTimeObj = new Carbon($bookingDateTime);
        $responseDateTimeObj = new Carbon($responseDateTime);

        if ($responseDateTimeObj->lt($bookingDateTimeObj)) {
            throw new \InvalidArgumentException("Response date time must be greater or equal to booking date.");
        }

        $bookingDayOfficeHours = $officeHours[$bookingDateTimeObj->dayOfWeek];

        // same day, may be different time
        if ($responseDateTimeObj->isSameDay($bookingDateTimeObj)) {

            // if closed there should be no booking made but let's say office hours have changed
            if ($bookingDayOfficeHours['isClosed']) {
                return 0;
            } else {
                $to = $responseDateTimeObj->min($responseDateTimeObj->copy()
                    ->setTimeFromTimeString($bookingDayOfficeHours['to']));
                $from = $bookingDateTimeObj->max($bookingDateTimeObj->copy()
                    ->setTimeFromTimeString($bookingDayOfficeHours['from']));

                return $to->gt($from) ? $to->diffInMinutes($from) : 0;
            }

        } else { // different day
            $minutesSum = 0;

            // how many minutes the office was opened on the booking day after the booking was made:
            if (!$bookingDayOfficeHours['isClosed']) {

                $bookingDayTo = $bookingDateTimeObj->copy()->setTimeFromTimeString($bookingDayOfficeHours['to']);
                $bookingDayFrom = $bookingDateTimeObj->max($bookingDateTimeObj->copy()
                    ->setTimeFromTimeString($bookingDayOfficeHours['from']));

                $minutesSum += $bookingDayTo->gt($bookingDayFrom) ? $bookingDayTo->diffInMinutes($bookingDayFrom) : 0;
            }

            $minutesOpen = $this->officeHoursToMinutes($officeHours);
            $minutesOpenInAWeek = array_sum($minutesOpen);
            $tmpDateTimeObj = $bookingDateTimeObj->copy()->addDay()->startOfDay();
            $startOfResponseDay = $responseDateTimeObj->copy()->startOfDay();

            // full weeks
            $noOfWeeks = $startOfResponseDay->diffInWeeks($tmpDateTimeObj);
            $minutesSum += $noOfWeeks * $minutesOpenInAWeek;

            $tmpDateTimeObj->addWeeks($noOfWeeks);

            // remaining days (less than one week)
            while ($tmpDateTimeObj->lt($startOfResponseDay)) {
                $minutesSum += $minutesOpen[$tmpDateTimeObj->dayOfWeek];
                $tmpDateTimeObj->addDay();
            }

            // how many minutes the office was opened on the response day before the response:
            $responseDayOfficeHours = $officeHours[$responseDateTimeObj->dayOfWeek];

            if (!$responseDayOfficeHours['isClosed']) {

                $responseDayTo = $responseDateTimeObj->min($responseDateTimeObj->copy()
                    ->setTimeFromTimeString($responseDayOfficeHours['to']));

                $responseDayFrom = $responseDateTimeObj->copy()
                    ->setTimeFromTimeString($responseDayOfficeHours['from']);

                $minutesSum += $responseDayTo->gt($responseDayFrom) ? $responseDayTo->diffInMinutes($responseDayFrom) : 0;
            }

            return $minutesSum;
        }


    }
}
