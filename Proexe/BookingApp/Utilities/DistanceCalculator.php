<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Illuminate\Support\Facades\DB;

class DistanceCalculator
{

    /**
     * @param array $from
     * @param array $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to, $unit = 'm')
    {

        $theta = $from[1] - $to[1];
        $dist = sin(deg2rad($from[0])) * sin(deg2rad($to[0])) + cos(deg2rad($from[0])) * cos(deg2rad($to[0])) * cos(deg2rad($theta));
        $dist = acos($dist);
        $distDeg = rad2deg($dist);

        $distMeters = $distDeg * 111189.57; // 1 degree = 111189.57 m

        $distance = $unit == 'km' ? $distMeters / 1000 : $distMeters;

        return round($distance);
    }

    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, $offices)
    {
//        PURE SQL SOLUTION Just uncomment these two lines and comment the ones after
//
//        $closestOfficeData = DB::select('SELECT *, MIN(acos(sin(RADIANS(?)) * sin(RADIANS(lat)) + cos(RADIANS(?)) * cos(RADIANS(lat)) * cos(RADIANS(? - lng)) )) as distance from offices group by id order by distance asc limit 1;', [$from[0], $from[0], $from[1]])[0];
//        $closestOffice = json_decode(json_encode($closestOfficeData), true);
//
//        END OF PURE SQL SOLUTION


        $closestOffice = $offices[0];
        $closestOfficeDist = $this->calculate($from, [$offices[0]['lat'], $offices[0]['lng']], 'm');

        $officeCount = count($offices);

        for ($i = 1; $i < $officeCount; $i++) {

            $office = $offices[$i];
            $distance = $this->calculate($from, [$office['lat'], $office['lng']], 'm');

            if ($distance < $closestOfficeDist) {
                $closestOffice = $office;
                $closestOfficeDist = $distance;
            }

        }

        return $closestOffice;
    }

}
